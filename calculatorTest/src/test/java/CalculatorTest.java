import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import java.text.MessageFormat;
import java.time.Duration;
import java.time.Instant;

import  static  org.junit.jupiter.api.Assertions.*;
public class CalculatorTest {

    private Calculator calculatorUnderTest;
    private static Instant startedAt;
    @BeforeAll
    public static void initStartingTime(){
        System.out.println("Appel avant tous les tests");
        startedAt= Instant.now();
    }
    @AfterAll
    public static void showTestDuration(){
        System.out.println("Appel aprés tous les tests");
        Instant endedAt=Instant.now();
        long duration= Duration.between(startedAt,endedAt).toMillis();
        System.out.println(MessageFormat.format("Durée des tests : {0} ms", duration));
    }
    @BeforeEach
     public void initCalculator(){
         calculatorUnderTest =new Calculator();
        System.out.println("Appel avant chaque Test");
    }
    @AfterEach
    public void undefCalculator(){
        System.out.println("Appel aprés chaque test");
        calculatorUnderTest=null; // désaffecter la variable pour éviter toute réutilisation
    }
    @Test
    void testAddTwoPositiveNumbers(){
        //Arrange
        int a =2;
        int b =3;
        //ACT
        int somme=calculatorUnderTest.add(a,b);
        //ASSERT
        assertEquals(5,somme);
    }
    @Test
    public void multiply_shouldReturnTheProduct_ofTwoIntegers(){
        //Arrange
        int a = 42;
        int b = 11;
        //Act
        int produit=calculatorUnderTest.multiply(a,b);
        //Assert
        assertEquals(462, produit);
    }
    @ParameterizedTest(name="{0} x 0 doit etre égal à 0 ")
    @ValueSource(ints = {1,2,42,1001,5089})
    public void multiply_shouldReturnZero_ofZeroWithMultipleIntegers( int arg){
        //Arrange - Tous est pret !
        //Act -- Multiplier par zéro
        int actualResult = calculatorUnderTest.multiply(arg, 0);
        //Assert -- ça vaut toujours zéro !
        assertEquals(0,actualResult);
    }
    @ParameterizedTest(name="{0} + {1} doit être égale à {2}")
    @CsvSource({"1,1,2", "2,3,5" , "42,57,99"})
    public void add_shouldReturnTheSum_ofMultipleIntegers(int arg1, int arg2, int expectResult){
        //Arrange - tout est pret !
        //Act
        int actualResult = calculatorUnderTest.add(arg1, arg2);
        //Assert
        assertEquals(expectResult, actualResult);
    }
    @Test
    @Timeout(1)
    public void longCalcul_shouldcomputeInLessThan1Second(){
        //Arrange
        //Act
        calculatorUnderTest.longCalculation();
        //Assert
        //...
    }
}
